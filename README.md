# H2 (NodeJS)

A client-server implementation using HTTP/2 implemented in NodeJS.

## Requirements

- Node.js >= 9.4.0

## Start the server
### HTTP/2
By default the server uses HTTP/2, start it by running either:
```bash
npm start
```

or

```bash
node index.js
```

And open: https://localhost:3000

### HTTP/1.1
One can start the server with only support for HTTP/1.1 by parsing the *legacy* flag `-l`:
```bash
node index.js -l
```

## Use the client
For a basic web request use:
```bash
node ./src/client.js
```

All client features can be viewed by executing:
```bash
node ./src/client.js -h
```

## Troubleshooting
#### The server is running, however I still cannot connect
Make sure that you try to connect to the right port and are using HTTPS. At this point there is no automatic redirect to upgrade HTTP requests to HTTPS.
