'use strict'

const fs = require('fs')
const path = require('path')
const mime = require('mime')
const nodeDir = require('node-dir')

function getFiles (baseDir) {
  const files = new Map()

  console.log('Indexing public files')
  nodeDir.files(baseDir, { sync: true }).forEach((absolutePath) => {
    const relativePath = path.relative(baseDir, absolutePath)
    const fileDescriptor = fs.openSync(absolutePath, 'r')
    const stat = fs.fstatSync(fileDescriptor)
    const contentType = mime.lookup(absolutePath)

    files.set(relativePath.replace(new RegExp('\\\\', 'g'), '/'), {
      path: absolutePath,
      fileDescriptor,
      headers: {
        'content-length': stat.size,
        'last-modified': stat.mtime.toUTCString(),
        'content-type': contentType
      }
    })
  })

  return files
}

module.exports = {
  getFiles
}
