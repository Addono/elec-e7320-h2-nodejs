'use strict'

const http2 = require('http2')
const fs = require('fs')
const path = require('path')
const argv = require('minimist')(process.argv.slice(2))
const notificationclient = require('./clients/notification-client')
const uploadimageclient = require('./clients/upload-image-client')
const tagclient = require('./clients/tag-client')
const getclient = require('./clients/get-client')

if (argv.h) {
  console.log(fs.readFileSync(path.join(__dirname, '../public/help-client.txt')).toString())
  return
}

const client = http2.connect('https://localhost:3000', {
  ca: fs.readFileSync(path.join(__dirname, '../ssl/localhost-cert.pem'))
})
client.on('error', (err) => console.error(err))

switch (argv.a) {
  case 'upload': {
    const fileName = argv.f

    if (fileName === undefined) {
      console.error("Expected file option to be set, please use the '-f $filename'")
      return
    }

    listenToRequest(uploadimageclient.post(client, fileName))
    break
  }
  case 'tag': {
    const key = argv.k
    const value = argv.v

    if (key === undefined) {
      console.error('Expected tags key and value option to be set, please use \'-k $key\'. \'-v $value\' can be ' +
        'optionally used to post another tag for this key')
      return
    }

    if (value === undefined) {
      listenToRequest(tagclient.request(client, key))
    } else {
      listenToRequest(tagclient.post(client, key, value))
    }
    break
  }
  case 'notify':
    listenToRequest(notificationclient.request(client))
    break
  case 'gentelella': {
    // First retrieves the index page, then loads all required resources synchronously.
    const push = argv.p
    listenToRequest(getclient.request(client, 'gentelella/index.html' + (push ? '?push' : ''), false), (_, data) => {
      console.log(`Received body of length: ${data.length}`)

      let paths = [
        'vendors/bootstrap/dist/css/bootstrap.min.css',
        'vendors/font-awesome/css/font-awesome.min.css',
        'vendors/nprogress/nprogress.css',
        'vendors/iCheck/skins/flat/green.css',
        'vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css',
        'vendors/jqvmap/dist/jqvmap.min.css',
        'vendors/bootstrap-daterangepicker/daterangepicker.css',
        'build/css/custom.min.css',
        'vendors/jquery/dist/jquery.min.js',
        'vendors/bootstrap/dist/js/bootstrap.min.js',
        'vendors/fastclick/lib/fastclick.js',
        'vendors/nprogress/nprogress.js',
        'vendors/Chart.js/dist/Chart.min.js',
        'vendors/gauge.js/dist/gauge.min.js',
        'vendors/bootstrap-progressbar/bootstrap-progressbar.min.js',
        'vendors/iCheck/icheck.min.js',
        'vendors/skycons/skycons.js',
        'vendors/Flot/jquery.flot.js',
        'vendors/Flot/jquery.flot.pie.js',
        'vendors/Flot/jquery.flot.time.js',
        'vendors/Flot/jquery.flot.stack.js',
        'vendors/Flot/jquery.flot.resize.js',
        'vendors/flot.orderbars/js/jquery.flot.orderBars.js',
        'vendors/flot-spline/js/jquery.flot.spline.min.js',
        'vendors/flot.curvedlines/curvedLines.js',
        'vendors/DateJS/build/date.js',
        'vendors/jqvmap/dist/jquery.vmap.js',
        'vendors/jqvmap/dist/maps/jquery.vmap.world.js',
        'vendors/jqvmap/examples/js/jquery.vmap.sampledata.js',
        'vendors/moment/min/moment.min.js',
        'vendors/bootstrap-daterangepicker/daterangepicker.js',
        'build/js/custom.min.js'
      ]

      // Retrieve all sub-resources, waiting for pushed resources when the argument is given.
      if (push) {
        // Wait for the resources to be pushed.
        client.on('stream', (pushedStream, requestHeaders) => {
          console.log(requestHeaders)
          let pushData = ''
          pushedStream.on('push', (responseHeaders) => {
            const pushStatusCode = responseHeaders[':status']
            if (pushStatusCode === 200) {
              console.log('Received push headers successfully')
            } else {
              console.log(`Push notification failed with error code ${pushStatusCode}`)
            }
          })
          pushedStream.on('data', (chunk) => {
            console.log('Receiving data')
            pushData += chunk
          })
          pushedStream.on('end', () => {
            console.log(`\n${pushData}`)
          })
        })
      } else {
        // Create new requests for each expected sub-resource
        paths.forEach((path) => {
          listenToRequest(getclient.request(client, path, false), (_, data) => {
            console.log(`Received sub-resource ${path} with size ${data.length}`)

            // Remove the current request from the queue.
            paths = paths.filter((value) => value !== path)

            // If this was the last request which finished, close the client connection.
            if (paths.length === 0) {
              client.close()
            }
          })
        })
      }
    })
    break
  }
  case 'get':
  default: {
    let path = argv.p
    if (path === undefined) {
      path = ''
    }

    listenToRequest(getclient.request(client, path))
    break
  }
}

/**
 * @param req The request object to be listened to.
 * @param callback function Function receiving the data received in the request.
 */
function listenToRequest (req, callback = undefined) {
  let headers
  req.on('response', (responseHeaders, flags) => {
    headers = responseHeaders
    for (const name in headers) {
      console.log(`${name}: ${headers[name]}`)
    }
  })

  req.setEncoding('utf8')
  let data = ''
  req.on('data', (chunk) => { data += chunk })
  req.on('end', () => {
    if (callback === undefined) {
      console.log(`\n${data}`)
    } else {
      callback(headers, data)
    }
  })
  req.end()
}

