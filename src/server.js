'use strict'

const fs = require('fs')
const path = require('path')
// eslint-disable-next-line
const argv = require('minimist')(process.argv.slice(2))
const https = require('https')
const http2 = require('http2')
const helper = require('./helper')

const USE_HTTP2 = !(argv.l || false) // Check if we should be using legacy HTTP/1.1
const { HTTP2_HEADER_PATH } = http2.constants
const PORT = process.env.PORT || 3000
const PUBLIC_PATH = path.join(__dirname, '..', 'public')
const IMAGES_PATH = path.join(PUBLIC_PATH, 'images')

const publicFiles = helper.getFiles(PUBLIC_PATH)
const server = createServer()({
  cert: fs.readFileSync(path.join(__dirname, '../ssl/localhost-cert.pem')),
  key: fs.readFileSync(path.join(__dirname, '../ssl/localhost-privkey.pem'))
}, onRequest)

const DB = { tags: {} }

function createServer () {
  if (USE_HTTP2) {
    console.log('Using HTTP/2')
    return http2.createSecureServer
  }

  console.log('Using HTTP/1')
  return https.createServer
}

// Push file
function push (res, path, delay = 0) {
  if (!USE_HTTP2) {
    console.log('Ignored server push for HTTP/1.1 request')
    return
  }

  if (Array.isArray(path)) {
    path.forEach((subpath) => push(res, subpath, delay))
    return
  }

  const file = publicFiles.get(path)

  if (!file) {
    console.error(`Couldn't find file ${file}`)
    return
  }

  res.stream.pushStream({ [HTTP2_HEADER_PATH]: `/${path}` }, (err, pushStream) => {
    if (err) {
      console.error(err)
      return
    }

    if (delay) {
      setTimeout(() => {
        pushStream.respondWithFD(file.fileDescriptor, file.headers)
      }, delay)
    } else {
      pushStream.respondWithFD(file.fileDescriptor, file.headers)
    }
  })
}

function respondWithFile (res, file) {
  // Write the headers
  res.writeHead(200, file.headers)

  // Write the file content
  fs.createReadStream(file.path).pipe(res)
}

// Request handler
function onRequest (req, res) {
  const reqPath = req.url === '/' ? '/index.html' : req.url

  switch (req.method) {
    case 'GET': {
      if (reqPath.startsWith('/tags/')) {
        const tagName = req.url.substr('/tags/'.length)
        let data
        let message

        if (tagName === '') {
          data = Object.keys(DB.tags)
          message = 'Listing all tags'
        } else {
          data = DB.tags[tagName]

          if (data === undefined) {
            message = 'No related tags found'
          } else {
            message = `Returning tags for ${tagName}`
          }
        }

        const body = JSON.stringify({
          message,
          data
        })

        res.statusCode = 200
        res.write(body)
        res.end()
        return
      }

      if (reqPath === '/images/' || reqPath === '/images') {
        fs.readdir(IMAGES_PATH, (err, files) => {
          if (err) {
            res.statusCode = 500
            res.write(`Failed reading image directory: ${err}`)
            return
          }

          const fileNames = []

          files
            .filter((file) => file.toLowerCase().endsWith('jpg') || file.toLowerCase().endsWith('png'))
            .forEach((file) => fileNames.push(file))

          const body = JSON.stringify({ images: fileNames })

          res.statusCode = 200
          res.write(body)
          res.end()
        })
        return
      }

      // Otherwise serve the file from the public folder
      const relativePath = reqPath.substring(1).split('?')[0]
      const file = publicFiles.get(relativePath)

      // File not found
      if (!file) {
        res.statusCode = 404
        res.end()
        return
      }

      if (reqPath === '/gentelella/index.html?push') {
        push(res, [
          'vendors/bootstrap/dist/css/bootstrap.min.css',
          'vendors/font-awesome/css/font-awesome.min.css',
          'vendors/nprogress/nprogress.css',
          'vendors/iCheck/skins/flat/green.css',
          'vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css',
          'vendors/jqvmap/dist/jqvmap.min.css',
          'vendors/bootstrap-daterangepicker/daterangepicker.css',
          'build/css/custom.min.css',
          'vendors/jquery/dist/jquery.min.js',
          'vendors/bootstrap/dist/js/bootstrap.min.js',
          'vendors/fastclick/lib/fastclick.js',
          'vendors/nprogress/nprogress.js',
          'vendors/Chart.js/dist/Chart.min.js',
          'vendors/gauge.js/dist/gauge.min.js',
          'vendors/bootstrap-progressbar/bootstrap-progressbar.min.js',
          'vendors/iCheck/icheck.min.js',
          'vendors/skycons/skycons.js',
          'vendors/Flot/jquery.flot.js',
          'vendors/Flot/jquery.flot.pie.js',
          'vendors/Flot/jquery.flot.time.js',
          'vendors/Flot/jquery.flot.stack.js',
          'vendors/Flot/jquery.flot.resize.js',
          'vendors/flot.orderbars/js/jquery.flot.orderBars.js',
          'vendors/flot-spline/js/jquery.flot.spline.min.js',
          'vendors/flot.curvedlines/curvedLines.js',
          'vendors/DateJS/build/date.js',
          'vendors/jqvmap/dist/jquery.vmap.js',
          'vendors/jqvmap/dist/maps/jquery.vmap.world.js',
          'vendors/jqvmap/examples/js/jquery.vmap.sampledata.js',
          'vendors/moment/min/moment.min.js',
          'vendors/bootstrap-daterangepicker/daterangepicker.js',
          'build/js/custom.min.js'
        ])
      }

      // Use push for notifications
      if (reqPath === '/notify.html') {
        push(res, 'notify.js', 5000)
      }

      if (reqPath === '/notify') {
        push(res, 'notification', 5000)
      }

      // Serve file
      if (argv.d && reqPath === '/vendors/jquery/dist/jquery.min.js') {
        console.log(`Delaying ${reqPath}`)
        setTimeout(() => respondWithFile(res, file), 500)
      } else {
        respondWithFile(res, file)
      }
      break
    }
    case 'POST': {
      if (reqPath.startsWith('/upload/image')) {
        const pathFromHeader = req.headers[':path']
        const imageName = getUrlParameter(pathFromHeader, 'name')
        const imageData = getUrlParameter(pathFromHeader, 'data')
        fs.writeFileSync(path.join(IMAGES_PATH, imageName), imageData, { encoding: 'binary' })

        res.statusCode = 200
      } else if (reqPath.startsWith('/upload/tag')) {
        const path = req.headers[':path']
        const key = getUrlParameter(path, 'key')

        if (DB.tags[key] === undefined) {
          DB.tags[key] = []
        }
        DB.tags[key].push(getUrlParameter(path, 'tag'))
      } else {
        res.statusCode = 404
      }
      res.end()
      break
    }
    default:
      console.error(`Request method ${req.method} not supported`)
      res.statusCode = 405
      res.end()
  }
}

function getUrlParameter (path, name) {
  name = name.replace(/[[]/, '\\[').replace(/[\]]/, '\\]')
  const regex = new RegExp(`[\\?&]${name}=([^&#]*)`)
  const results = regex.exec(path)
  return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '))
}

server.listen(PORT, (err) => {
  if (err) {
    console.error(err)
    return
  }

  console.log(`Server listening on ${PORT}`)
  console.log(`Access the server at https://localhost:${PORT}`)
})
