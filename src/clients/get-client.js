'use strict'

function request (client, path, close = true) {
  const req = client.request({ ':path': `/${path}` })

  req.on('end', () => {
    if (close) {
      client.close()
    }
  })

  return req
}

module.exports = {
  request
}
