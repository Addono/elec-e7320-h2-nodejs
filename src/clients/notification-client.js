'use strict'

function request (client) {
  const req = client.request({ ':path': '/notify' })

  client.on('stream', (pushedStream, requestHeaders) => {
    let pushData = ''
    pushedStream.on('push', (responseHeaders) => {
      const pushStatusCode = responseHeaders[':status']
      if (pushStatusCode === 200) {
        console.log('Received push headers successfully')
      } else {
        console.log(`Push notification failed with error code ${pushStatusCode}`)
      }
    })
    pushedStream.on('data', (chunk) => {
      pushData += chunk
    })
    pushedStream.on('end', () => {
      console.log(`\n${pushData}`)
      client.close()
    })
  })

  return req
}

module.exports = {
  request
}
