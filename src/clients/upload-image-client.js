'use strict'

const fs = require('fs')
const path = require('path')

function post (client, imagePath) {
  const imageData = fs.readFileSync(path.join(__dirname, '../..', imagePath), { encoding: 'binary' })
  const encodedImageData = encodeURIComponent(imageData)
  const imageName = path.basename(imagePath)

  const req = client.request({
    ':path': `/upload/image?data=${encodedImageData}&name=${imageName}`,
    ':method': 'POST'
  })

  req.on('end', () => {
    client.close()
  })

  return req
}

module.exports = {
  post
}
