'use strict'

function request (client, key) {
  const req = client.request({ ':path': `/tags/${key}` })
  req.on('end', () => {
    client.close()
  })
  return req
}

function post (client, key, value) {
  const req = client.request({
    ':path': `/upload/tag?key=${key}&tag=${value}`,
    ':method': 'POST'
  })

  req.on('end', () => {
    client.close()
  })

  return req
}

module.exports = {
  request,
  post
}
